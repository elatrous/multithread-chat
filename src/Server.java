

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Iterator;

public class Server {
    public static volatile ArrayList<String> names = new ArrayList<String>();
    public static void main(String[] args) throws IOException,SocketException {
        try {
            int port = 9000;
            int gport = 9001; // ports for the groups
            InetAddress group = InetAddress.getByName("224.0.0.1");
            ServerSocket server = new ServerSocket(port);
            System.out.println("Server started on port " + port);
            DatagramSocket socket = new DatagramSocket(port);

            new ServerThread(socket,group,gport).start();
        }
        catch(Exception e) {
            System.out.println(e);
        }
    }

    static synchronized void addName(String name) {
        names.add(name);
    }
    static synchronized void removeName(String name) {
        Iterator itr = names.iterator(); 
        while (itr.hasNext()) 
        { 
            String s = (String)itr.next(); 
            if (s.contains(name)) 
                itr.remove(); 
        }
    }
}

class ServerThread extends Thread {
    private DatagramSocket socket;
    private InetAddress group;
    private int port;
    
    public ServerThread(DatagramSocket s, InetAddress g, int p) {
        socket = s;
        group = g;
        port = p;


    }
    public void run() {
        try {
            byte[] buf;
            DatagramPacket packetIn, packetOut;
            String message;
            while(true) {
                buf = new byte[256];
                packetIn = new DatagramPacket(buf,buf.length);
                socket.receive(packetIn);
                message = new String(packetIn.getData());
                if (message.startsWith("name")) {
                    String name = message.split(":")[1];
                    if (!isUnique(name)) {
                        String alt = "rejected";
                        buf = alt.getBytes();
                        packetOut = new DatagramPacket(buf,buf.length, packetIn.getAddress(), packetIn.getPort());
                        socket.send(packetOut);
                    }
                    else {
                        Server.addName(name);
                        String alt = "accepted";
                        buf = alt.getBytes();
                        packetOut = new DatagramPacket(buf,buf.length, packetIn.getAddress(), packetIn.getPort());
                        socket.send(packetOut);
                        message = name.concat(" has joined the chat");
                        buf = new byte[512];
                        buf = message.getBytes();
                        packetOut = new DatagramPacket(buf,buf.length, group, port);
                        socket.send(packetOut);
                    }
                }
                else if (message.contains("exit")) {
                    String name = message.split(":")[0];
                    Server.removeName(name);
                    message = name.concat(" has left the chat");
                    buf = new byte[512];
                    buf = message.getBytes();
                    packetOut = new DatagramPacket(buf,buf.length, group, port);
                    socket.send(packetOut);
                    //return;
                }
                else {
                    buf = message.getBytes();
                    packetOut = new DatagramPacket(buf,buf.length, group, port);
                    socket.send(packetOut);
                }
            }
        }
        catch(Exception e) {
            System.out.println(e);
        }
    }

    private Boolean isUnique(String name) {
        for(String s: Server.names){
			if(name.contains(s))
				return false;
		}
		return true;
    }
     
}