
import java.io.*;
import java.net.*;

public class Client {
    public static void main(String[] args) throws IOException {
        DatagramSocket socket = new DatagramSocket(0);
        InetAddress server = InetAddress.getLocalHost();

        MulticastSocket multicastSocket = new MulticastSocket(9001);
        InetAddress group = InetAddress.getByName("224.0.0.1");
        multicastSocket.joinGroup(group);

        //Console console = System.console();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
        byte[] buf = new byte[256];
        DatagramPacket packetIn, packetOut;
        String message = "null", name;

        // getting name from client
        do{
            //name = console.readLine("Enter a unique name: ");
        	System.out.print("Enter a unique name: ");
            name = reader.readLine();
        	message = "name:" + name;
            buf = message.getBytes();
            packetOut = new DatagramPacket(buf,buf.length,server,9000);
            socket.send(packetOut);
            buf = new byte[256];
            packetIn = new DatagramPacket(buf,buf.length);
            socket.receive(packetIn);
            message = new String(packetIn.getData());
            System.out.println(message);
        } while(!message.contains("accepted"));

        new ReadThread(multicastSocket).start();
        new WriteThread(socket,multicastSocket,group,server,9000,name).start();

    }
}
// make the write thread reference this for it to end
class ReadThread extends Thread {
    private MulticastSocket socket;
    
    public ReadThread(MulticastSocket s) {
        socket = s;
    }
    public void run() {
        try {
            while(true) {
                byte[] buf = new byte[512];
                DatagramPacket packet = new DatagramPacket(buf,buf.length);
                socket.receive(packet);
                String message = new String(packet.getData());
                System.out.println(message);
            }
        }
        catch(Exception e) {
            System.out.println(e);
        }
    }
}
class WriteThread extends Thread {
    private DatagramSocket socket;
    private MulticastSocket multicastSocket;
    private InetAddress group;
    private InetAddress server;
    private int port;
    private String name;
    
    public WriteThread(DatagramSocket s,MulticastSocket ms,InetAddress g, InetAddress sv, int p, String n) {
        socket = s;
        multicastSocket = ms;
        group = g;
        server = sv;
        port = p;
        name = n;
    }
    public void run() {
        try {
            //Console console = System.console();
        	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
            String message="null", msg;

            do {
                //message = console.readLine();
            	message = reader.readLine();
                msg = name + ": " + message;
                byte[] buf = msg.getBytes();
                DatagramPacket packet = new DatagramPacket(buf,buf.length,server,port);
                socket.send(packet);
            } while (!message.equals("exit"));

            socket.close();
            multicastSocket.leaveGroup(group);
            multicastSocket.close();
        }
        catch(Exception e) {
            System.out.println(e);
        }
    }
}